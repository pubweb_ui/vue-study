import Notice from '@/views/Notice.vue'
import NoticeList from '@/components/notice/NoticeList.vue'
import NoticeView from '@/components/notice/NoticeView.vue'
import Error404 from '@/components/common/Error404.vue'

const routes = [
	{
		path: '/',
		name: 'home',
		meta: { title: '메인' },
		redirect: { name: 'notice_list', query: { page: '1' } }
	},
	{
		path: '/notice',
		name: 'notice',
		meta: { title: '공지사항' },
		component: Notice,
		redirect: { name: 'notice_list', query: { page: '1' } },
		children: [
			{
				path: 'view/:id',
				name: 'notice_view',
				meta: { title: '공지사항 글 보기' },
				component: NoticeView,
				props: true
			},
			{
				path: 'list',
				name: 'notice_list',
				meta: { title: '공지사항 글 목록' },
				component: NoticeList
			}
		]
	},
	{
		path: '/404',
		name: '404',
		component: Error404
	},
	{
		// 페이지가 없을 경우 404 화면으로 이동하도록
		path: '*',
		redirect: { name: '404' }
	}
]

export default routes