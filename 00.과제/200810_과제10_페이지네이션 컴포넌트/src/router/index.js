import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter)

// 새로운 라우트로 이동할 때 스크롤 위치를 최상단으로 초기화
// url에 hash가 있다면 셀렉터로 스크롤 이동
// 뒤로/앞으로 가기라면 저장된 위치로 스크롤 이동
const scrollBehavior = (to, from, savedPosition) => {
	if (savedPosition) {
		return savedPosition
	} else if (to.hash) {
		return { selector: to.hash }
	}
	return { x: 0, y: 0 }
}

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	linkActiveClass: 'on',
	routes,
	scrollBehavior
})

export default router